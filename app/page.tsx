"use client"
import React, { useState, useEffect } from 'react'
import Sidebar from "../components/Sidebar";
import HomePage from "../components/HomePage";
import AboutPage from "../components/AboutPage";
import ServicePage from "../components/ServicePage";
import PortfolioPage from "../components/PortfolioPage";
import ContactPage from "../components/ContactPage";
import Switcher from "../components/Switcher";

export default function Home() {
  const [href, setHref] = useState("")
  const [prevHref, setPrevHref] = useState("")

  const onClickSidebar = (url: string) => {
    setPrevHref(href);
    setHref(url)
  }

  useEffect(() => {
    setHref("home")
  },[]);
  

  useEffect(() => {
    const allSection = document.querySelectorAll(".section");
    allSection.forEach(i => {
      
      if(href === i.id) i.classList.toggle("active")
      else {
        console.log(href, i.id)
        i.classList.remove("active")
      }

      if (prevHref === i.id) i.classList.toggle("back-section")
        else i.classList.remove("back-section")
    })
  },[href, prevHref]);

  return (
    <div id='Home'>
      <Sidebar href={href} onClickSidebar={onClickSidebar}/>
      <div className='main-content'> 
        <HomePage onClickSidebar={onClickSidebar}/>
        <AboutPage onClickSidebar={onClickSidebar}/>
        <ServicePage/>
        <PortfolioPage/>
        <ContactPage/>
        <Switcher/>
      </div>
    </div>
  );
}
