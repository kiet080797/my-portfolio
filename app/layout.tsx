import { Inter } from "next/font/google"
import "./globals.css";
import '@fortawesome/fontawesome-free/css/all.min.css'
import { Metadata } from 'next'
import { Analytics } from "@vercel/analytics/react"

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Kiet's portfolio",
  description: "I'm a Web Developer with extensive experience for over 2 years. My experience is to create and develop website, fix bug and many more...",
  icons: {
    icon: "/favicon.ico",
    shortcut: "/favicon.ico",
  },
  openGraph: {
    title: "Kiet's portfolio",
    description: "I'm a Web Developer with extensive experience for over 2 years. My experience is to create and develop website, fix bug and many more...",
    images: [
      {
        url: "https://www.facebook.com/photo/?fbid=2719064951577512&set=a.109237725893594",
        height: 600,
        alt: 'My avatar',
      },
    ],
  },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        {children}
        <Analytics/>
      </body>
    </html>
  );
}
