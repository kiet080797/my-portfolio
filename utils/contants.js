export const HREF = {
  ABOUT: 'about',
  HOME: 'home',
  SERVICE: 'service',
  PORTFOLIO: 'portfolio',
  CONTACT: 'contact',
};
