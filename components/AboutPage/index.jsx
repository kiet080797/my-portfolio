import React from 'react'
import Link from 'next/link'
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const AboutPage = ({onClickSidebar}) => {

  return (
    <div>
        <section className="about section" id="about">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>About Me</h2>
              </div>
            </div>
            <div className="row">
              <div className="about-content padding-15">
                <div className="row">
                  <div className="about-text padding-15">
                    <h3>I'm Kiet Nguyen and <span>Web Developer</span></h3>
                    <ul>
                      <li>Have more than 2 years of experience in Front-end (ReactJS)</li>
                      <li> Have basic knowledge of Back-end (NestJS)</li>
                      <li>Experienced on web development in various different genres since 2021.</li>
                      <li>Good working experience includes: Object-oriented methodology, developing projects in ReactJS, NextJS, Typescript, Javascript, HTML, CSS, Redux, Hooks, HOC, JS/ReactJS with Jest, RESTful API, JSON.</li>
                      <li>High-pressure working environments.</li>
                      <li>Confident to communicate with customers on working projects and systems</li>
                    </ul>
                  </div>
                </div>
                <div className="row">
                  <div className="personal-info padding-15">
                    <div className="row">
                      <div className="info-item padding-15">
                        <p>Birthday : <span>08 July 1997</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Age : <span>27</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Facebook : <span>kietnguyen.0807</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Email : <span>kiet080797@gmail.com</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Degree : <span>Computer Science</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Phone : <span>+84 347 551 122</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>City : <span>Ho Chi Minh</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Freelance : <span>Available</span></p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="buttons padding-15">
                        <Link href="https://www.topcv.vn/download-cv?cv_id=XAZaAAMKAloJV1IGWVRUUlICDQMCD1VbAggGVQf9ba&dontcount=1" target="_blank" className="btn">Download CV</Link>
                        <Link href="#contact" className="btn" onClick={()=> onClickSidebar(HREF.CONTACT)}>Contact Me</Link>
                      </div>
                    </div>
                  </div>
                  <div className="skills padding-15">
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>JS</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "86%" }}></div>
                          <div className="skill-percent">86%</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>Java</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "20%" }}></div>
                          <div className="skill-percent">20%</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>HTML</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "96%" }}></div>
                          <div className="skill-percent">96%</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>CSS</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "76%" }}></div>
                          <div className="skill-percent">76%</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="education padding-15">
                    <h3 className="title">Education</h3>
                    <div className="row">
                      <div className="timeline-box padding-15">
                        <div className="timeline shadow-dark">
                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>2015 - 2020
                            </h3>
                            <h4 className="timeline-title">University of Information Technology (UIT)</h4>
                            <div className="timeline-major"> <i>Major: Computer Science</i></div>
                            <p className="timeline-text">
                              The University of Information Technology (UIT), 
                              a member of the Vietnam National University Ho Chi Minh City (VNU-HCM), 
                              is a public university specializing in information technology and communication (IT&C). 
                              It was established under Decision No. 134/2006/QD-TTg dated June 8 2006, by the Prime Minister of the Government.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="experience padding-15">
                    <h3 className="title">Experience</h3>
                    <div className="row">
                      <div className="timeline-box padding-15">
                        <div className="timeline shadow-dark">
                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>July 2020 - Dec 2020
                            </h3>
                            <h4 className="timeline-title">VUA DU COMPANY</h4>
                            <div className="timeline-major"> <i>Intern Front-end</i></div>
                            <div>
                            <h4 className="timeline-text-title">Project: Ecommerce Website</h4>
                              <p className="timeline-text">
                                The website is built to sell various types of bakeries online that are ordered on the website and the online payment process.
                              </p>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Developed Frontend using ReactJS</li>
                                <li>Daily report process working to Mentor, Scrum Master / PO</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Technologies used in project:</h4>
                              <ul className="timeline-text-list">
                                <li>ReactJS, React hook, Javascript, HTML/CSS, ThemeUI</li>
                                <li>Strapi</li>
                                <li>GitHub</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Links:</h4>
                              <ul className="timeline-text-list">
                                <li>Website:</li>
                                <div><i>bakery-app.vercel.app</i></div>
                                <li>UI source:</li>
                                <div><i>gitlab.com/kiet080797/bakery-app</i></div>
                                <li>API source:</li>
                                <div><i>github.com/kiet0807/server-bakery-app</i></div>
                              </ul>
                            </div>
                          </div>

                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>May 2021 - Oct 2021
                            </h3>
                            <h4 className="timeline-title">PAVN LIMITED COMPANY</h4>
                            <div className="timeline-major"> <i>Fresher Front-end</i></div>
                            <div>
                              <h4 className="timeline-text-title">Project: Service Website</h4>
                              <p className="timeline-text">
                                The website was built to meet the needs of buying and selling online services on the website.
                              </p>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Developed Frontend using NextJS</li>
                                <li>Daily report process working to Mentor, Scrum Master / PO</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Technologies used in project:</h4>
                              <ul className="timeline-text-list">
                                <li>NextJS, React hook, Javascript, HTML/CSS, Material UI</li>
                                <li>GitLab</li>
                              </ul>
                            </div>
                          </div>

                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>Nov 2022 - Jan 2024
                            </h3>
                            <h4 className="timeline-title">TMA SOLUTION AND GBST COMPANY</h4>
                            <div className="timeline-major"> <i>Junior Front-end</i></div>
                            <div>
                              <h4 className="timeline-text-title">Project: Wealth Management</h4>
                              <p className="timeline-text">
                                This extensive global software platform serves Platforms, Wraps, Unit Trusts, ISAs, OEICs, SIPPs, and Pensions. 
                                It provides a cohesive, customer-centric application, ensuring a smooth, user-friendly, and flexible solution.
                              </p>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Implement new features</li>
                                <li>Analyze and clarify requirements with customer</li>
                                <li>Develop flexible and high-performance user interface</li>
                                <li>Create and modify the product UI</li>
                                <li>Fix bugs</li>
                                <li>Write unit tests</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Technologies used in project:</h4>
                              <ul className="timeline-text-list">
                                <li>ReactJS, EmberJS</li>
                                <li>ReactUI, Formik, Bootstrap</li>
                                <li>MicroFrontend</li>
                                <li>REST API</li>
                                <li>IntelliJ IDE, Jira, Jenkin</li>
                              </ul>
                            </div>
                          </div>
                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>Jan 2024 - May 2024
                            </h3>
                            <h4 className="timeline-title">FREELANCER</h4>
                            <div className="timeline-major"> <i>Fullstack</i></div>
                            <div>
                            <h4 className="timeline-text-title">Project: Blog Website</h4>
                              <p className="timeline-text">
                                This is a basic website for managing and storing your users and posts with a combination of ReactJS, NextJS and NestJS.
                              </p>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Build API system with NestJS, typeORM and MySQL</li>
                                <li>Build an admin dashboard management website with ReactJS</li>
                                <li>Build a client website to showcase your users and posts</li>
                                <li>Fix bugs</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Technologies used in project:</h4>
                              <ul className="timeline-text-list">
                                <li>NestJS, ReactJS, NextJS, Typescript</li>
                                <li>Bootstrap, Redux, React hook form</li>
                                <li>Docker, MySQL</li>
                                <li>JWT Swagger, REST API</li>
                                <li>GitHub</li>
                              </ul>
                            </div>
                            <div>
                              <h4 className="timeline-text-title">Links:</h4>
                              <ul className="timeline-text-list">
                                <li>UI source: </li>
                                <div><i>github.com/kiet0807/blog-nextjs</i></div>
                                <li>UI admin source: </li>
                                <div><i>github.com/kiet0807/blog-reactjs</i></div>
                                <li>API source: </li>
                                <div><i>github.com/kiet0807/blog-nestjs</i></div>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default AboutPage
