import React, { useEffect } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import Avatar from '@/public/img/avatar.jpg'
import Typed from 'typed.js';
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const HomePage = ({onClickSidebar}) => {

  useEffect(() => {
    const typed = new Typed(".typing", {
      strings: ["Web Developer", "Software Engineer", "Front-end Developer"],
      typeSpeed: 100,
      backSpeed: 60,
      loop: true
    });

    return () => {
      typed.destroy();
    };
  }, []);

  return (
    <div>
        <section className="home section active" id="home">
          <div className="container">
            <div className="row">
              <div className="home-info padding-15">
                <h3 className="hello">Hello, my name is  <span className="name">Kiet Nguyen</span></h3>
                <h3 className="my-profession">I'm a <span className="typing">Web Developer</span></h3>
                <p>I'm a Web Developer with extensive experience for over 2 years. My experience is to create and develop website, fix bug and many more... </p>
                <Link href='#about' className="btn" onClick={()=> onClickSidebar(HREF.ABOUT)}>More About Me</Link>
              </div>
              <div className="home-img padding-15">
               <Image src={Avatar} className="img" height={400} alt=""/>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}


export default HomePage
