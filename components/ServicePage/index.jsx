import React from 'react'
import './style.css'
import '@/Responsive/responsive.css'

const ServicePage = () => {
  return (
    <div>
        <section className="service section" id="service">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>Skills</h2>
              </div>
            </div>
            <div className="row">
              <div className="service-item padding-15">
                  <div className="service-item-inner">
                    <div className="icon">
                      <div className="fa fa-mobile-alt"></div>
                    </div>
                    <h4>Developing Mobile App</h4>
                    <p>This is the process of creating mobile applications, designed and built to run on mobile devices such as smartphones and tablets</p>
                  </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-laptop-code"></div>
                  </div>
                  <h4>Developing Website</h4>
                  <p>This is the process of creating websites, from designing the interface to deploying and maintaining a functioning website</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-palette"></div>
                  </div>
                  <h4>Web Design and App Design</h4>
                  <p>These are both processes of designing user interfaces (UI) and user experiences (UX) for web and mobile applications, respectively</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-code"></div>
                  </div>
                  <h4>Convert interface from images to HTML CSS</h4>
                  <p>This is the process of converting visual designs or models (usually in image formats such as PSD, Sketch or Figma) into actual web pages using HTML and CSS code</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-search"></div>
                  </div>
                  <h4>Research Skill</h4>
                  <p>It is the ability to gather information, analyze data, and logically infer from various sources to achieve a specific goal</p>
                </div>
              </div>
              <div className="service-item padding-15">
              <div className="service-item-inner">
                <div className="icon">
                  <div className="fa fa-bullhorn"></div>
                </div>
                <h4>Interpersonal Skill</h4>
                <p>It is the ability to communicate and interact effectively with others in a variety of situations and environments</p>
              </div>
             </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default ServicePage
