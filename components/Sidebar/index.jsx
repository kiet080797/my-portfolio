import React, { useEffect, useState, useRef } from 'react'
import Link from 'next/link'
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from "@/utils/contants"
import { useOnClickOutside } from 'usehooks-ts'

const Sidebar = ({href, onClickSidebar}) => {
  const ref = useRef(null);
  const [openSidebar, setOpenSideBar] = useState(false);
  
  useEffect(() => {
    const allSection = document.querySelectorAll(".section");
    const aside = document.querySelector(".aside");
    const navToggler = document.querySelector(".nav-toggler");
    if(openSidebar) {
      allSection?.forEach(i => i.classList.toggle("open"));
      aside.classList.toggle("open");
      navToggler.classList.toggle("open");
    } else {
      allSection?.forEach(i => i.classList.remove("open"));
      aside.classList.remove("open");
      navToggler.classList.remove("open");
    }
  },[openSidebar]);

  const handleSidebar = (url) => {
    onClickSidebar(url);
    if(href !== url) setOpenSideBar(false);
  }

  useOnClickOutside(ref, () => setOpenSideBar(false))
  return (
    <div>
      <div ref={ref} className="aside">
        <div className="logo" onClick={()=> handleSidebar(HREF.HOME)}>
            <Link href="#home"><span>P</span>ortfolio</Link>
        </div>
        <div className="nav-toggler" onClick={()=> setOpenSideBar(prev => !prev)}><span></span></div>
        <ul className="nav">
            <li onClick={()=> handleSidebar(HREF.HOME)}>
              <Link href="#home" className={href === HREF.HOME? "link active": "link"}><i className="fa fa-home"></i>Home</Link>
              </li>
            <li onClick={()=> handleSidebar(HREF.ABOUT)}>
              <Link href="#about" className={href === HREF.ABOUT? "link active": "link"}><i className="fa fa-user"></i>About</Link>
              </li>
            <li onClick={()=> handleSidebar(HREF.SERVICE)}>
              <Link href="#service" className={href === HREF.SERVICE? "link active": "link"}><i className="fa fa-list"></i>Skills</Link>
              </li>
            <li onClick={()=> handleSidebar(HREF.PORTFOLIO)}>
              <Link href="#portfolio" className={href === HREF.PORTFOLIO? "link active": "link"}><i className="fa fa-briefcase"></i>Portfolio</Link>
              </li>
            <li onClick={()=> handleSidebar(HREF.CONTACT)}>
              <Link href="#contact" className={href === HREF.CONTACT? "link active": "link"}><i className="fa fa-comment"></i>Contact</Link>
            </li>
        </ul>
      </div>
    </div>
  )
}

export default Sidebar
