import React, { useState, useRef } from 'react'
import './style.css'
import { useOnClickOutside } from 'usehooks-ts'

const Switcher = () => {
  const ref = useRef(null);
  const [darkMode, setDarkMode] = useState(false);
  const [openColors, setOpenColors] = useState(false);
 
  const changeColor = (newColor) => {
    document.documentElement.style.setProperty('--skin-color', newColor);
  }
  const changeDarkMode = () => {
    if(darkMode) document.body.classList.remove('dark');
    else document.body.classList.add('dark');
    setDarkMode(prev => !prev);
  }

  useOnClickOutside(ref, () => setOpenColors(false))
  return (
    <div ref={ref} className={openColors ? "style-switcher open" : "style-switcher"} >
        <div className="style-switcher-toggler s-icon" onClick={() => setOpenColors(prev => !prev)}>
          <i className="fas fa-cog fa-spin"></i>
        </div>
        <div className="day-night s-icon">
          <i className={darkMode ? "fas fa-sun" : "fas fa-moon"} onClick={changeDarkMode}></i>
        </div>
        <h4>Theme Colors</h4>
        <div className="colors">
          <span className="color-1" onClick={() => changeColor('#ec1839')}></span>
          <span className="color-2" onClick={() => changeColor('#fa5b0f')}></span>
          <span className="color-3" onClick={() => changeColor('#37b182')}></span>
          <span className="color-4" onClick={() => changeColor('#1854b4')}></span>
          <span className="color-5" onClick={() => changeColor('#f021b2')}></span>
        </div>
    </div>
  )
}

export default Switcher

